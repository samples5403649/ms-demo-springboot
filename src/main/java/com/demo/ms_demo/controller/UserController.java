package com.demo.ms_demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import com.demo.ms_demo.dto.UserDTO;
import com.demo.ms_demo.exception.BusinessException;
import com.demo.ms_demo.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/sign-up")
    public ResponseEntity<?> signUp(@RequestBody UserDTO user) throws BusinessException {
    	System.out.println("signup:INI");
        UserDTO registeredUser = userService.signUp(user);
        return new ResponseEntity<>(registeredUser, HttpStatus.CREATED);
    }

    @GetMapping("/login")
    public ResponseEntity<?> login(@RequestHeader(name = "Authorization") String token) {
    	System.out.println("login:INI");
        UserDTO loggedInUser = userService.login(token);
        Authentication authentication = new UsernamePasswordAuthenticationToken(loggedInUser.getEmail(), loggedInUser.getPassword());
        String newToken = userService.generateToken(authentication);
        loggedInUser.setToken(newToken);
        return new ResponseEntity<>(loggedInUser, HttpStatus.OK);
    }
}

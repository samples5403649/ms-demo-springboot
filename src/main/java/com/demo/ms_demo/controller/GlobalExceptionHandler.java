package com.demo.ms_demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.demo.ms_demo.dto.ErrorResponseDTO;
import com.demo.ms_demo.exception.BusinessException;

import java.util.Date;

@RestControllerAdvice
public class GlobalExceptionHandler {

	/***
	 * Exeption genérica
	 * @param e
	 * @return
	 */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponseDTO> handleException(Exception e) {
    	e.printStackTrace();
        ErrorResponseDTO errorResponse = new ErrorResponseDTO(new Date(), HttpStatus.INTERNAL_SERVER_ERROR.value(), "Se ha producido un error interno del servidor. revisa el formato JSON del request");
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }


	/***
	 * Exeption de negocio
	 * @param e
	 * @return
	 */
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ErrorResponseDTO> handleBusinessException(Exception e) {
    	e.printStackTrace();
        ErrorResponseDTO errorResponse = new ErrorResponseDTO(new Date(), HttpStatus.BAD_REQUEST.value(), e.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

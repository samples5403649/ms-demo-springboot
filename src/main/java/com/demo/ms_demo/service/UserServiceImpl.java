package com.demo.ms_demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.demo.ms_demo.config.JwtTokenProvider;
import com.demo.ms_demo.dto.PhoneDTO;
import com.demo.ms_demo.dto.UserDTO;
import com.demo.ms_demo.exception.BusinessException;
import com.demo.ms_demo.model.PhoneModel;
import com.demo.ms_demo.model.UserModel;
import com.demo.ms_demo.respository.UserRepository;

import java.util.Date;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {
	private static final String EMAIL_REGEX ="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
	 private static final String PASSWORD_REGEX ="^(?=.*[A-Z])(?=.*[0-9].*[0-9])(?=.*[a-z]).{8,12}$";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDTO signUp(UserDTO userDTO) throws BusinessException {
    	System.out.println("email["+userDTO.getEmail()+"]");
        validateUser(userDTO);
        
        // Configurar la fecha de creación y último inicio de sesión
        Date now = new Date();
        userDTO.setCreated(now);
        userDTO.setLastLogin(now);
        userDTO.setActive(true);
        
        UserModel userModel = new UserModel();
        userModel.setEmail(userDTO.getEmail());
        userModel.setName(userDTO.getName());
        userModel.setCreated(now);
        userModel.setLastLogin(now);
        userModel.setActive(true);
        
        userDTO.getPhones().forEach(pDTO->{
        	PhoneModel pModel = new PhoneModel();
        	pModel.setCitycode(pDTO.getCitycode());
        	pModel.setCountrycode(pDTO.getCountrycode());
        	pModel.setId(pDTO.getId());
        	pModel.setNumber(pDTO.getNumber());
        });
        // Encriptar la contraseña antes de almacenarla en la base de datos
        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));


        // Guardar el usuario en la base de datos
        userRepository.save(userModel);
        userDTO.setId(userModel.getId());

        // Generar y retornar el token JWT para el nuevo usuario
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDTO.getEmail(), userDTO.getPassword());
        String token = jwtTokenProvider.generateToken(authentication);
        System.out.println("token["+token+"]");
        userDTO.setToken(token);

        return userDTO;
    }

    @Override
    public String generateToken(Authentication authentication) {
        return jwtTokenProvider.generateToken(authentication);
    }

    @Override
    public UserDTO login(String token) {
        String username = jwtTokenProvider.getUsernameFromToken(token);

        Optional<UserModel> uModel = userRepository.findByEmail(username);
        
        if (uModel.isPresent()) {
        	Date now = new Date();
        	
            UserModel user = uModel.get();
            user.setLastLogin(now);
            userRepository.save(user);
            
            UserDTO uDTO= new UserDTO();

            uDTO.setEmail(username);
            uDTO.setId(user.getId());
            uDTO.setName(user.getName());
            uDTO.setToken(token);
            uDTO.setLastLogin(now);
            uDTO.setActive(user.isActive());
            
            user.getPhones().forEach(pModel->{
            	PhoneDTO pDTO = new PhoneDTO();
            	pDTO.setCitycode(pModel.getCitycode());
            	pDTO.setCountrycode(pModel.getCountrycode());
            	pDTO.setId(pModel.getId());
            	pDTO.setNumber(pModel.getNumber());
            });
            
            return uDTO;
        } else {
            throw new RuntimeException("Usuario no encontrado");
        }
    }

    private void validateUser(UserDTO user) throws BusinessException {
    	System.out.println("userRepository["+userRepository+"] "+user.getEmail());

        // Verifica si el usuario ya existe en la base de datos
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new BusinessException("El usuario ya existe");
        }
        
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        
        if(!pattern.matcher(user.getEmail()).matches()) {
            throw new BusinessException("Formato de email inválido");        	
        }
        
        pattern = Pattern.compile(PASSWORD_REGEX);
        
        if(!pattern.matcher(user.getPassword()).matches()) {
            throw new BusinessException("Formato de contraseña incorrecto");        	
        }
    }
}

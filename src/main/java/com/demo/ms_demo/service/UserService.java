package com.demo.ms_demo.service;

import org.springframework.security.core.Authentication;

import com.demo.ms_demo.dto.UserDTO;
import com.demo.ms_demo.exception.BusinessException;

public interface UserService {

	UserDTO signUp(UserDTO user)throws BusinessException ;

    String generateToken(Authentication authentication);

    UserDTO login(String token);
}

-- schema.sql

CREATE TABLE IF NOT EXISTS user (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    created DATETIME,
    lastLogin DATETIME
);

CREATE TABLE IF NOT EXISTS phone (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    number BIGINT,
    citycode INT,
    countrycode VARCHAR(10),
    user_id BIGINT,
    FOREIGN KEY (user_id) REFERENCES user(id)
);

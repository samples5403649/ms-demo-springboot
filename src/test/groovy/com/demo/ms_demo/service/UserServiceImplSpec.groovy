import org.springframework.security.core.Authentication;
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Shared
import org.springframework.security.crypto.password.PasswordEncoder
import com.demo.ms_demo.config.JwtTokenProvider
import com.demo.ms_demo.dto.UserDTO
import com.demo.ms_demo.model.UserModel
import com.demo.ms_demo.respository.UserRepository
import com.demo.ms_demo.service.UserServiceImpl;
import com.demo.ms_demo.exception.BusinessException;

class UserServiceImplSpec extends Specification {

    UserServiceImpl userService

    UserRepository userRepository = Mock(UserRepository)
    JwtTokenProvider jwtTokenProvider = Mock(JwtTokenProvider)
    PasswordEncoder passwordEncoder = Mock(PasswordEncoder)

    def setup() {
        userService = new UserServiceImpl(
            passwordEncoder: passwordEncoder
        )

		userService.jwtTokenProvider=jwtTokenProvider
		userService.userRepository=userRepository
    }

    def "signUp should create and save a new user"() {
        given: "a user dto dor input"
        def userDTO = new UserDTO(
            name: "John Doe",
            email: "john@example.com",
            password: "a2asfGfdfdf4",
            phones: []
        )
        and: "a mock for jwt token provider that returns a 'gerenatedToken' String "
        
        1 * jwtTokenProvider.generateToken(_) >> "generatedToken"
		and: "a mock for user repository foundByEmail function that returns an empty Optional"
        1 * userRepository.findByEmail(_) >> Optional.empty()
        and: "a mock for user repository save function that returns id 100 for the new record"
        1 * userRepository.save(_) >> { UserModel userModel -> userModel.setId(100) }

        when: "the function userService.signUp is called"
        def result = userService.signUp(userDTO)

        then: "The returned dto has the same values than the input, and the id 100"
        result.name == "John Doe"
        result.email == "john@example.com"
        result.token == "generatedToken"
        result.id==100
    }


    def "signUp should throw exception if user already exists"() {
        given:
        def userDTO = new UserDTO(
            name: "John Doe",
            email: "john@example.com",
            password: "a2asfGfdfdf4",
            phones: []
        )

        and:
        userRepository.findByEmail(_) >> Optional.of(new UserModel())

        when:
        def result = userService.signUp(userDTO)

        then:
        def e=thrown (BusinessException)
        e.message=="El usuario ya existe"
    }


    def "signUp should throw exception if email format is invalid"() {
        given:
        def userDTO = new UserDTO(
            name: "John Doe",
            email: "john@examplecom",
            password: "a2asfGfdfdf4",
            phones: []
        )

        and:
        userRepository.findByEmail(_) >> Optional.empty()

        when:
        def result = userService.signUp(userDTO)

        then:
        def e=thrown (BusinessException)
        e.message=="Formato de email inválido"
    }


    def "signUp should throw exception if password format is invalid"() {
        given:
        def userDTO = new UserDTO(
            name: "John Doe",
            email: "john@example.com",
            password: "password123",
            phones: []
        )

        and:
        userRepository.findByEmail(_) >> Optional.empty()

        when:
        def result = userService.signUp(userDTO)

        then:
        def e=thrown (BusinessException)
        e.message=="Formato de contraseña incorrecto"
    }


    def "login should return stored data given a token as input"() {
        given:
        1 * jwtTokenProvider.getUsernameFromToken(_) >> "john@example.com"
        and:
        1 * userRepository.findByEmail(_) >> { Optional.of(new UserModel(id:100, phones:new ArrayList()))}

        when:
        def result = userService.login("john@example.com")

        then:
        result.id==100
    }
    

}

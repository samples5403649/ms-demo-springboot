# Proyecto de microservicio
## Setup
- Java 1.8  Se utiliza LAMBDA para el foreach de teléfonos en UserServiceImpl, También se utiliza Generic en UserDTO en la lista de teléfonos
- Gradle 7.4
- Springboot 2.5.14
- Spring Security ha sido desactivado debido a que no existen endpoints con restricción de acceso

## Ejecución del proyecto.
### Base de datos.
La base de datos es cargada en memoria y el script /resources/schema.sql es cargado cada vez que se inicia la aplicación, esta carga se especifica en el archivo application.properties

## ejecución
## test unitarios
- Ejecutar `./gradlew test`

## coverge de tests
Losa tests se han realizado con spock2.3-groovy4.0. El coverage incluye el 80% solo del servicio
- Ejecutar `./gradlew clean test jacocoTestReport`
- Revisar el reporte en la ruta `build/reports/jacoco/test/html/index.html`

### sign-up
Llamada curl
```
curl -X POST \
  http://localhost:8080/api/users/sign-up \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "Usuario Ejemplo",
    "email": "usuario@example.com",
    "password": "PasswordSeguro123",
    "phones": [
      {
        "number": 123456789,
        "citycode": 56,
        "contrycode": "CL"
      }
    ]
  }'
```

### login
Llamada curl

```
curl -X GET \
  http://localhost:8080/api/users/login \
  -H 'Content-Type: application/json' \
  -H 'Authorization: TOKEN'
```

##Diagrams
### Components

![alt text](UML/Component Diagram1.jpg "Component Diagram")

### Sequence
![alt text](UML/Sequence ms-demo.jpg "Sequence Diagram")
